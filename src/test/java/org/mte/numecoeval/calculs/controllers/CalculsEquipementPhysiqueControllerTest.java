package org.mte.numecoeval.calculs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.controllers.CalculsEquipementPhysiqueController;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapperImpl;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.HypotheseRest;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CalculsEquipementPhysiqueControllerTest {

    CalculsEquipementPhysiqueController calculsEquipementPhysiqueController;

    @BeforeEach
    public void setup() {
        calculsEquipementPhysiqueController = new CalculsEquipementPhysiqueController(
                new DomainMapperImpl(),
                new ObjectMapper(),
                new DureeDeVieEquipementPhysiqueServiceImpl()
        );
    }

    @Test
    void withValidInput_shouldBeOK() {
        DemandeCalculImpactEquipementPhysiqueRest demandeCalcul = getValidInput("FIN_DE_VIE");

        var result = assertDoesNotThrow(() -> calculsEquipementPhysiqueController.calculerImpactEquipementPhysique(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("OK", indicateur.getStatutIndicateur());
        assertNotNull(indicateur.getImpactUnitaire());
    }

    @Test
    void withInvalidInput_responseShouldBeOKWithSpecialResponse() {
        DemandeCalculImpactEquipementPhysiqueRest demandeCalcul = getValidInput("FIN_DE_VIE");
        // Sans date et sans hypothèse, le calcul ne se fait pas
        demandeCalcul.getEquipementPhysique().setDateAchat(null);
        demandeCalcul.getEquipementPhysique().setDateRetrait(null);
        demandeCalcul.setHypotheses(Collections.emptyList());
        demandeCalcul.getTypeEquipement().setDureeVieDefaut(null);

        var result = assertDoesNotThrow(() -> calculsEquipementPhysiqueController.calculerImpactEquipementPhysique(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("ERREUR", indicateur.getStatutIndicateur());
        assertNull(indicateur.getImpactUnitaire());
        assertTrue(indicateur.getTrace().contains("\"ErrCalcFonc : La durée de vie par défaut de l'équipement n'a pas pu être déterminée\""));
    }

    private static DemandeCalculImpactEquipementPhysiqueRest getValidInput(String etape) {
        var demandeCalcul = Instancio.of(DemandeCalculImpactEquipementPhysiqueRest.class).create();
        demandeCalcul.getCorrespondanceRefEquipement().setModeleEquipementSource(demandeCalcul.getEquipementPhysique().getModele());
        demandeCalcul.getEtape().setCode(etape);
        demandeCalcul.getCritere().setNomCritere("Changement climatique");
        demandeCalcul.getEquipementPhysique().setDateAchat(LocalDate.of(2018,1,1));
        demandeCalcul.getEquipementPhysique().setDateRetrait(LocalDate.of(2023,1,1));
        demandeCalcul.getEquipementPhysique().setPaysDUtilisation("France");
        demandeCalcul.getEquipementPhysique().getDataCenter().setLocalisation("France");
        demandeCalcul.getEquipementPhysique().getDataCenter().setNomCourtDatacenter(demandeCalcul.getEquipementPhysique().getNomCourtDatacenter());

        demandeCalcul.getImpactEquipements().forEach(impactEquipementRest -> {
            impactEquipementRest.setRefEquipement(demandeCalcul.getCorrespondanceRefEquipement().getRefEquipementCible());
            impactEquipementRest.setEtape(demandeCalcul.getEtape().getCode());
            impactEquipementRest.setCritere(demandeCalcul.getCritere().getNomCritere());
            impactEquipementRest.setType(demandeCalcul.getEquipementPhysique().getType());
        });
        demandeCalcul.getMixElectriques().forEach(mixElectriqueRest -> {
            mixElectriqueRest.setCritere(demandeCalcul.getCritere().getNomCritere());
            mixElectriqueRest.setPays(demandeCalcul.getEquipementPhysique().getPaysDUtilisation());
            mixElectriqueRest.setPays(demandeCalcul.getEquipementPhysique().getDataCenter().getLocalisation());
        });


        demandeCalcul.setHypotheses(Arrays.asList(
                HypotheseRest.builder().code("dureeDeVieParDefaut").valeur(5.0).build(),
                HypotheseRest.builder().code("test").valeur(1.0).build()
        ));
        return demandeCalcul;
    }
}
