package org.mte.numecoeval.calculs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.infrastructure.controllers.CalculsEquipementVirtuelController;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapperImpl;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculEquipementVirtuelRest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class CalculsEquipementVirtuelControllerTest {

    CalculsEquipementVirtuelController calculsEquipementVirtuelController;

    @BeforeEach
    public void setup() {
        calculsEquipementVirtuelController = new CalculsEquipementVirtuelController(
                new DomainMapperImpl(),
                new ObjectMapper()
        );
    }

    @Test
    void withValidInput_shouldReturnOK() {
        var demandeCalcul = getValidInput();

        var result = assertDoesNotThrow( () -> calculsEquipementVirtuelController.calculerImpactEquipementVirtuel(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("OK", indicateur.getStatutIndicateur());
    }

    @Test
    void withInvalidInput_shouldReturnBadRequest() {
        var demandeCalcul = getValidInput();
        // Si le statut de l'indicateur est null, il n'y aura pas de calcul d'indicateur
        demandeCalcul.getImpactEquipement().setStatutIndicateur("ERREUR");

        var result = assertDoesNotThrow( () -> calculsEquipementVirtuelController.calculerImpactEquipementVirtuel(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("ERREUR", indicateur.getStatutIndicateur());
        assertTrue(indicateur.getTrace().contains("L'indicateur d'impact équipement associé est au statut : ERREUR"));
    }

    private static DemandeCalculEquipementVirtuelRest getValidInput() {
        var demandeCalcul = Instancio.of(DemandeCalculEquipementVirtuelRest.class).create();
        demandeCalcul.getImpactEquipement().setNomEquipement(demandeCalcul.getEquipementVirtuel().getNomEquipementPhysique());
        demandeCalcul.getImpactEquipement().setEtapeACV("FABRICATION");
        demandeCalcul.getImpactEquipement().setCritere("Changement climatique");
        demandeCalcul.getImpactEquipement().setStatutIndicateur("OK");

        return demandeCalcul;
    }
}
