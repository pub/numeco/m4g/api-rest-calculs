package org.mte.numecoeval.calculs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.infrastructure.controllers.CalculsApplicationController;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapperImpl;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculApplicationRest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class CalculsApplicationControllerTest {

    CalculsApplicationController controller;

    @BeforeEach
    public void setup() {
        controller = new CalculsApplicationController(
                new DomainMapperImpl(),
                new ObjectMapper()
        );
    }

    @Test
    void withValidInput_shouldReturnOK() {
        var demanceCalcul = getValidInput();

        var result = assertDoesNotThrow( () -> controller.calculerImpactApplication(demanceCalcul) );

        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("OK", indicateur.getStatutIndicateur());
    }

    @Test
    void withInvalidInput_shouldReturnBadResquest() {
        var demanceCalcul = getValidInput();
        // Indicateur en erreur si l'indicateur parent est en erreur
        demanceCalcul.getImpactEquipementVirtuel().setStatutIndicateur("ERREUR");

        var result = assertDoesNotThrow( () -> controller.calculerImpactApplication(demanceCalcul) );

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("ERREUR", indicateur.getStatutIndicateur());
        assertTrue(indicateur.getTrace().contains("L'indicateur d'impact équipement virtuel associé est au statut : ERREUR"));
    }

    private static DemandeCalculApplicationRest getValidInput() {
        var demandeCalcul = Instancio.of(DemandeCalculApplicationRest.class).create();
        demandeCalcul.getImpactEquipementVirtuel().setEtapeACV("FABRICATION");
        demandeCalcul.getImpactEquipementVirtuel().setCritere("Changement climatique");
        demandeCalcul.getImpactEquipementVirtuel().setStatutIndicateur("OK");
        return demandeCalcul;
    }
}
