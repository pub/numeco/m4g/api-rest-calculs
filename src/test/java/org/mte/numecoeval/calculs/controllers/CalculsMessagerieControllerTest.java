package org.mte.numecoeval.calculs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.infrastructure.controllers.CalculsMessagerieController;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapperImpl;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculMessagerieRest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class CalculsMessagerieControllerTest {

    CalculsMessagerieController controller;

    @BeforeEach
    public void setup() {
        controller = new CalculsMessagerieController(
                new DomainMapperImpl(),
                new ObjectMapper()
        );
    }

    @Test
    void withValidInput_shouldReturnOK() {
        var demandeCalcul = getValidInput();

        var result = assertDoesNotThrow(() -> controller.calculerImpactMessagerie(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
    }

    @Test
    void withInvalidInput_shouldReturnError() {
        var demandeCalcul = getValidInput();
        // Pas d'impact messagerie
        demandeCalcul.setImpactsMessagerie(null);

        var result = assertDoesNotThrow(() -> controller.calculerImpactMessagerie(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
    }

    private static DemandeCalculMessagerieRest getValidInput() {
        var demandeCalcul = Instancio.of(DemandeCalculMessagerieRest.class).create();
        demandeCalcul.getCritere().setNomCritere("Changement climatique");

        demandeCalcul.getImpactsMessagerie().forEach(impactMessagerieRest -> {
            impactMessagerieRest.setCritere(demandeCalcul.getCritere().getNomCritere());
        });
        demandeCalcul.getMessagerie().setMoisAnnee(12022);

        return demandeCalcul;
    }
}
