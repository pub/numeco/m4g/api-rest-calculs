package org.mte.numecoeval.calculs.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.calculs.domain.data.erreur.TypeErreurCalcul;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactRuntimeException;
import org.mte.numecoeval.calculs.infrastructure.controllers.ExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ExceptionHandlerTest {

    ExceptionHandler exceptionHandler = new ExceptionHandler();

    @Mock
    WebRequest webRequest;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void whenCalculImpactExceptionIsThrown_shouldReturnMatchingErreurRest() {
        CalculImpactException exception = new CalculImpactException(
                TypeErreurCalcul.ERREUR_TECHNIQUE.getCode(),
                "Erreur de tests"
        );

        var result = exceptionHandler.calculImpactException(exception, webRequest);

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getStatus());
        assertEquals(HttpStatus.BAD_REQUEST.name(), result.getCode());
        assertEquals(LocalDate.now(), result.getTimestamp().toLocalDate());
        assertEquals(exception.getLocalizedMessage(), result.getMessage());
    }

    @Test
    void whenCalculImpactRuntimeExceptionIsThrown_shouldReturnMatchingErreurRest() {
        CalculImpactRuntimeException exception = new CalculImpactRuntimeException(
                TypeErreurCalcul.ERREUR_TECHNIQUE.getCode(),
                "Erreur de tests"
        );

        var result = exceptionHandler.calculImpactRuntimeException(exception, webRequest);

        assertNotNull(result);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getStatus());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.name(), result.getCode());
        assertEquals(LocalDate.now(), result.getTimestamp().toLocalDate());
        assertEquals(exception.getLocalizedMessage(), result.getMessage());
    }

    @Test
    void whenGeneralExceptionIsThrown_shouldReturnMatchingErreurRest() {
        var exception = new Exception(
                "Erreur de tests"
        );

        var result = exceptionHandler.exception(exception, webRequest);

        assertNotNull(result);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getStatus());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.name(), result.getCode());
        assertEquals(LocalDate.now(), result.getTimestamp().toLocalDate());
        assertEquals("Erreur interne de traitement lors du traitement de la requête", result.getMessage());
    }

    @Test
    void whenGeneralRuntimeExceptionIsThrown_shouldReturnMatchingErreurRest() {
        var exception = new NullPointerException(
                "Erreur de tests"
        );

        var result = exceptionHandler.runtimeException(exception, webRequest);

        assertNotNull(result);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getStatus());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.name(), result.getCode());
        assertEquals(LocalDate.now(), result.getTimestamp().toLocalDate());
        assertEquals("Erreur interne de traitement lors du traitement de la requête", result.getMessage());
    }
}
