package org.mte.numecoeval.calculs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.infrastructure.controllers.CalculsReseauController;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapperImpl;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactReseauEquipementPhysiqueRest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class CalculsReseauControllerTest {

    CalculsReseauController controller;

    @BeforeEach
    void setup() {
        controller = new CalculsReseauController(
                new DomainMapperImpl(),
                new ObjectMapper()
        );
    }

    @Test
    void withValidInput_shouldReturnOK() {
        var demandeCalcul = getValidInput();

        var result = assertDoesNotThrow( () -> controller.calculerImpactReseau(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("OK", indicateur.getStatutIndicateur());
    }

    @Test
    void withInvalidInput_shouldReturnErreur() {
        var demandeCalcul = getValidInput();
        demandeCalcul.getImpactsReseau().forEach(impactReseauRest -> {
            impactReseauRest.setRefReseau("INVALID_TEST");
        });

        var result = assertDoesNotThrow( () -> controller.calculerImpactReseau(demandeCalcul));

        assertNotNull(result);
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        var indicateur = result.getBody();
        assertNotNull(indicateur);
        assertEquals("ERREUR", indicateur.getStatutIndicateur());
        assertTrue(indicateur.getTrace().contains("La référence ImpactReseau n'existe pas."));
    }

    private static DemandeCalculImpactReseauEquipementPhysiqueRest getValidInput() {
        var demandeCalcul = Instancio.of(DemandeCalculImpactReseauEquipementPhysiqueRest.class).create();
        demandeCalcul.getCritere().setNomCritere("Changement climatique");
        demandeCalcul.getEtape().setCode("FABRICATION");

        demandeCalcul.getImpactsReseau().forEach(impactReseauRest -> {
            impactReseauRest.setCritere(demandeCalcul.getCritere().getNomCritere());
            impactReseauRest.setEtapeACV(demandeCalcul.getEtape().getCode());
            impactReseauRest.setRefReseau(CalculImpactReseauService.REF_RESEAU);
        });

        return demandeCalcul;
    }
}
