package org.mte.numecoeval.calculs;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = { "test" })
class ApiRestCalculsApplicationTest {

    @Test
    void testContextLoadOK(@Autowired Environment environment) {
        assertNotNull(environment, "L'environnement est correctement chargé");
    }
}
