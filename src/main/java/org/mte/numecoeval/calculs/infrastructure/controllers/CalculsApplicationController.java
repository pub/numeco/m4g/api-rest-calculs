package org.mte.numecoeval.calculs.infrastructure.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicationServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculApplicationRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactApplicationRest;
import org.mte.numecoeval.rest.calculs.generated.api.server.CalculsApplicationApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsApplicationController implements CalculsApplicationApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    @Override
    public ResponseEntity<IndicateurImpactApplicationRest> calculerImpactApplication(DemandeCalculApplicationRest demandeCalculApplicationRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculApplicationRest);

        var indicateur = new CalculImpactApplicationServiceImpl(
                objectMapper
        ).calculImpactApplicatif(demandeCalcul);

        if(!"OK".equals(indicateur.getStatutIndicateur())) {
            return ResponseEntity.badRequest()
                    .body(domainMapper.toRest(indicateur));
        }

        return ResponseEntity.ok(
                domainMapper.toRest(indicateur)
        );
    }
}
