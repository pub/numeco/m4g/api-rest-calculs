package org.mte.numecoeval.calculs.infrastructure.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculEquipementVirtuelRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactEquipementVirtuelRest;
import org.mte.numecoeval.rest.calculs.generated.api.server.CalculsEquipementVirtuelApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsEquipementVirtuelController implements CalculsEquipementVirtuelApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    @Override
    public ResponseEntity<IndicateurImpactEquipementVirtuelRest> calculerImpactEquipementVirtuel(DemandeCalculEquipementVirtuelRest demandeCalculEquipementVirtuelRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculEquipementVirtuelRest);

        var resultat = new CalculImpactEquipementVirtuelServiceImpl(
                objectMapper
        ).calculerImpactEquipementVirtuel(demandeCalcul);

        if(!"OK".equals(resultat.getStatutIndicateur())) {
            return ResponseEntity.badRequest().body(
                    domainMapper.toRest(resultat)
            );
        }

        return ResponseEntity.ok(
                domainMapper.toRest(resultat)
        );
    }
}
