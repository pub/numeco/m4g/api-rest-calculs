package org.mte.numecoeval.calculs.infrastructure.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.server.CalculsEquipementPhysiqueApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class CalculsEquipementPhysiqueController implements CalculsEquipementPhysiqueApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService;

    @Override
    public ResponseEntity<IndicateurImpactEquipementPhysiqueRest> calculerImpactEquipementPhysique(DemandeCalculImpactEquipementPhysiqueRest demandeCalculImpactEquipementPhysiqueRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculImpactEquipementPhysiqueRest);

        var resultat = new CalculImpactEquipementPhysiqueServiceImpl(
                dureeDeVieEquipementPhysiqueService,
                objectMapper
        ).calculerImpactEquipementPhysique(demandeCalcul);

        if(!"OK".equals(resultat.getStatutIndicateur())) {
            return ResponseEntity.badRequest().body(
                    domainMapper.toRest(resultat)
            );
        }

        return ResponseEntity.ok(
                domainMapper.toRest(resultat)
        );
    }
}
