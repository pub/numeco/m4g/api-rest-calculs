package org.mte.numecoeval.calculs.infrastructure.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactReseauEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactReseauRest;
import org.mte.numecoeval.rest.calculs.generated.api.server.CalculsReseauApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsReseauController implements CalculsReseauApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    @Override
    public ResponseEntity<IndicateurImpactReseauRest> calculerImpactReseau(DemandeCalculImpactReseauEquipementPhysiqueRest demandeCalculImpactReseauEquipementPhysiqueRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculImpactReseauEquipementPhysiqueRest);

        var resultat = new CalculImpactReseauServiceImpl(
                objectMapper
        ).calculerImpactReseau(demandeCalcul);

        if(!"OK".equals(resultat.getStatutIndicateur())) {
            return ResponseEntity.badRequest().body(
                    domainMapper.toRest(resultat)
            );
        }

        return ResponseEntity.ok(
                domainMapper.toRest(resultat)
        );
    }
}
