package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicationService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicationServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {

    @Bean
    DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService() {
        return new DureeDeVieEquipementPhysiqueServiceImpl();
    }

    @Bean
    CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService(@Autowired ObjectMapper objectMapper) {
        return new CalculImpactEquipementPhysiqueServiceImpl(dureeDeVieEquipementPhysiqueService(), objectMapper);
    }

    @Bean
    CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService(@Autowired ObjectMapper objectMapper) {
        return new CalculImpactEquipementVirtuelServiceImpl(objectMapper);
    }

    @Bean
    CalculImpactApplicationService calculImpactApplicationService(@Autowired ObjectMapper objectMapper) {
        return new CalculImpactApplicationServiceImpl(objectMapper);
    }

    @Bean
    CalculImpactReseauService calculImpactReseauService(@Autowired ObjectMapper objectMapper) {
        return new CalculImpactReseauServiceImpl(objectMapper);
    }

    @Bean
    CalculImpactMessagerieService calculImpactMessagerieService(@Autowired ObjectMapper objectMapper) {
        return new CalculImpactMessagerieServiceImpl(objectMapper);
    }
}
