package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactReseau;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactApplication;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactReseau;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCorrespondanceRefEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielTypeEquipement;
import org.mte.numecoeval.rest.calculs.generated.api.model.ApplicationRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.CorrespondanceRefEquipementRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.CritereRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DataCenterRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculApplicationRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculEquipementVirtuelRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculImpactReseauEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculMessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.EquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.EquipementVirtuelRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.EtapeRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.HypotheseRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.ImpactEquipementRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.ImpactMessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.ImpactReseauRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactApplicationRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactEquipementPhysiqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactEquipementVirtuelRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactMessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactReseauRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.MessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.MixElectriqueRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.TypeEquipementRest;

@Mapper(componentModel = "spring")
public interface DomainMapper {

    /*Entrées*/
    EquipementPhysique toDomain(EquipementPhysiqueRest rest);
    DataCenter toDomain(DataCenterRest rest);

    EquipementVirtuel toDomain(EquipementVirtuelRest rest);

    Application toDomain(ApplicationRest rest);

    Messagerie toDomain(MessagerieRest rest);

    EquipementPhysiqueRest toRest(EquipementPhysique domain);
    DataCenterRest toRest(DataCenter domain);

    @Mapping(source = "VCPU",target = "vCPU")
    EquipementVirtuelRest toRest(EquipementVirtuel domain);

    ApplicationRest toRest(Application domain);

    MessagerieRest toRest(Messagerie domain);

    /*Référentiels*/

    ReferentielTypeEquipement toDomain(TypeEquipementRest rest);

    ReferentielCorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementRest rest);

    ReferentielHypothese toDomain(HypotheseRest rest);

    ReferentielEtapeACV toDomain(EtapeRest rest);
    ReferentielCritere toDomain(CritereRest rest);

    ReferentielImpactEquipement toDomain(ImpactEquipementRest rest);

    @Mapping(source = "valeur", target = "impactReseauMobileMoyen")
    ReferentielImpactReseau toDomain(ImpactReseauRest rest);

    ReferentielMixElectrique toDomain(MixElectriqueRest rest);

    ReferentielImpactMessagerie toDomain(ImpactMessagerieRest rest);

    TypeEquipementRest toRest(ReferentielTypeEquipement domain);

    CorrespondanceRefEquipementRest toRest(ReferentielCorrespondanceRefEquipement domain);

    HypotheseRest toRest(ReferentielHypothese domain);

    EtapeRest toRest(ReferentielEtapeACV domain);

    CritereRest toRest(ReferentielCritere domain);

    ImpactEquipementRest toRest(ReferentielImpactEquipement domain);

    @Mapping(source = "impactReseauMobileMoyen", target = "valeur")
    ImpactReseauRest toRest(ReferentielImpactReseau domain);

    MixElectriqueRest toRest(ReferentielMixElectrique domain);

    /*Indicateurs*/
    ImpactEquipementPhysique toDomain(IndicateurImpactEquipementPhysiqueRest rest);

    ImpactEquipementVirtuel toDomain(IndicateurImpactEquipementVirtuelRest rest);

    ImpactApplication toDomain(IndicateurImpactApplicationRest rest);

    IndicateurImpactEquipementPhysiqueRest toRest(ImpactEquipementPhysique domain);

    IndicateurImpactEquipementVirtuelRest toRest(ImpactEquipementVirtuel domain);

    IndicateurImpactApplicationRest toRest(ImpactApplication domain);

    IndicateurImpactReseauRest toRest(ImpactReseau domain);

    IndicateurImpactMessagerieRest toRest(ImpactMessagerie domain);

    /*Demandes*/
    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactEquipementPhysique toDomain(DemandeCalculImpactEquipementPhysiqueRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactReseau toDomain(DemandeCalculImpactReseauEquipementPhysiqueRest rest);


    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactEquipementVirtuel toDomain(DemandeCalculEquipementVirtuelRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactApplication toDomain(DemandeCalculApplicationRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactMessagerie toDomain(DemandeCalculMessagerieRest rest);
}
