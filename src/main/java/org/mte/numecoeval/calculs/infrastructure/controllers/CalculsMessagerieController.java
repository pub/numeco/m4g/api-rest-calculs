package org.mte.numecoeval.calculs.infrastructure.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.rest.calculs.generated.api.model.DemandeCalculMessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.model.IndicateurImpactMessagerieRest;
import org.mte.numecoeval.rest.calculs.generated.api.server.CalculsMessagerieApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsMessagerieController implements CalculsMessagerieApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    @Override
    public ResponseEntity<IndicateurImpactMessagerieRest> calculerImpactMessagerie(DemandeCalculMessagerieRest demandeCalculMessagerieRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculMessagerieRest);

        var resultat = new CalculImpactMessagerieServiceImpl(
                objectMapper
        ).calculerImpactMessagerie(demandeCalcul);

        if(!"OK".equals(resultat.getStatutIndicateur())) {
            return ResponseEntity.badRequest()
                    .body(domainMapper.toRest(resultat));
        }

        return ResponseEntity.ok(
                domainMapper.toRest(resultat)
        );
    }
}
