package org.mte.numecoeval.calculs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestCalculsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestCalculsApplication.class, args);
	}

}
