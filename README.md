# /!\ Ce repository est obsolète

Le nouveau repository se trouve ici : https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval

# api-expositionDonneesEntrees

API permettant d'envoyer des données d'entrées dans le système NumEcoEval.
L'import de données peut se faire via des fichiers CSV respectant un format spécifique ou au format JSON.
Le contrat d'interface est disponible au format [OpenAPI 3](src/main/resources/static/openapi.yaml).

## Pré-requis

- JDK 17
  - [Open JDK](https://jdk.java.net/java-se-ri/17)
  - [Coretto](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
- [Maven 3](https://maven.apache.org/download.cgi)
- Un IDE compatible avec Maven 3 et JDK 17

## Build

Pour compiler l'application, utilisez la commande suivante :

```bash
mvn clean install
```

### API REST et Contrat OpenAPI

Le contrat OpenAPI 3 est disponible dans les ressources du projet : [openapi.yaml](src/main/resources/static/openapi.yaml)

Ce contrat est utilisé pour générer les interfaces et POJOs utilisés dans les communications REST via le plugin Maven [openapi-generator-maven-plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-maven-plugin)

#### Profiles Maven

- DEPENDENCY-CHECK : Un profile permettant un scan local des dépendances Maven avec le dependency-check Maven plugin d'OWASP.

### Configuration de l'application

Editer le fichier [application.yml](src/main/resources/application.yaml)

### Démarrer l'application

Pour démarrer l'application avec la configuration actuelle, utilisez la commande suivante:

```bash
mvn spring-boot:run
```

Avec la configuration par défaut, l'API REST sera disponible sur l'URL suivante : [http://localhost:18081](http://localhost:18081)

L'application utilise Springdoc-openapi permettant un affichage via Swagger-UI sur l'URL [http://localhost:18081/api/swagger-ui/index.html](http://localhost:18081/api/swagger-ui/index.html).
* Par défaut, le contrat d'interface OpenAPI est également disponible sur l'URL [http://localhost:18081/openapi.yaml](http://localhost:18081/openapi.yaml).

## Licences
[Apache 2.0](LICENSE.txt)

## Liens utiles

- [Spring Documentation (version courante)](https://docs.spring.io/spring-framework/docs/current/reference/html/)
- [Spring Boot Documentation (version courante)](https://docs.spring.io/spring-boot/docs/current/reference/html/index.html)